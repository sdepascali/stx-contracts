pragma solidity ^0.4.25;


import "openzeppelin-solidity/contracts/math/Math.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC20/SafeERC20.sol";


contract WhitelistInterface {
  function hasRole(address _operator, string _role) public view returns (bool);
}


contract WhitelistProxyInterface {
  function whitelist() public view returns (WhitelistInterface);
}


contract Exchange {
  using SafeERC20 for IERC20;
  using SafeMath for uint256;

  uint256 public constant POINTS = uint256(10) ** 32;

  address public constant WHITELIST_PROXY_ADDRESS = 0x77eb36579e77e6a4bcd2Ca923ada0705DE8b4114;  // 0xec8be1a5630364292e56d01129e8ee8a9578d7d8;
  WhitelistProxyInterface public whitelistProxy = WhitelistProxyInterface(WHITELIST_PROXY_ADDRESS);

  struct Order {
    bool buy;
    uint256 closingTime;
    uint256 numberOfTokens;
    uint256 priceWei;
    IERC20 token;
    address from;
  }

  mapping(bytes32 => Order) public orders;

  event OrderDeleted(bytes32 indexed order);
  event OrderFilled(bytes32 indexed order, uint256 numberOfTokens, uint256 priceWei, address indexed to);
  event OrderPosted(bytes32 indexed order, bool indexed buy, uint256 closingTime, uint256 numberOfTokens, uint256 priceWei, IERC20 indexed token, address from);

  function deleteOrder(bytes32 _hash) public {
    Order memory o = orders[_hash];
    require(o.from == msg.sender || !isValid(_hash));

    if (o.buy)
      o.from.transfer(o.priceWei);

    _deleteOrder(_hash);
  }

  function fillOrders(bytes32[] _hashes, uint256 numberOfTokens) public payable {
    uint256 remainingTokens = numberOfTokens;
    uint256 remainingWei = msg.value;

    for (uint256 i = 0; i < _hashes.length; i++) {
      bytes32 hash = _hashes[i];
      require(isValid(hash));

      Order memory o = orders[hash];

      uint256 coefficient = (o.buy ? remainingTokens : remainingWei).mul(POINTS).div(o.buy ? o.numberOfTokens : o.priceWei);

      uint256 nTokens = o.numberOfTokens.mul(Math.min(coefficient, POINTS)).div(POINTS);
      uint256 vWei = o.priceWei.mul(Math.min(coefficient, POINTS)).div(POINTS);

      o.buy ? remainingTokens -= nTokens : remainingWei -= vWei;

      (o.buy ? msg.sender : o.from).transfer(vWei);
      o.token.safeTransferFrom(o.buy ? msg.sender : o.from, o.buy ? o.from : msg.sender, nTokens);
      emit OrderFilled(hash, nTokens, vWei, msg.sender);

      _deleteOrder(hash);

      if (coefficient < POINTS)
        _postOrder(o.buy, o.closingTime, o.numberOfTokens.sub(nTokens), o.priceWei.sub(vWei), o.token, o.from);
    }

    msg.sender.transfer(remainingWei);
  }

  function isValid(bytes32 _hash) public view returns (bool valid) {
    Order memory o = orders[_hash];

    valid = o.numberOfTokens > 0 && o.priceWei > 0 && now <= o.closingTime && o.closingTime <= now.add(1 weeks);
    valid = valid && whitelistProxy.whitelist().hasRole(o.token, "authorized");
    valid = valid && (o.buy || (o.token.balanceOf(o.from) >= o.numberOfTokens && o.token.allowance(o.from, address(this)) >= o.numberOfTokens));
  }

  function postOrder(bool _buy, uint256 _closingTime, uint256 _numberOfTokens, uint256 _priceWei, IERC20 _token) public payable {
    require(!_buy || _priceWei == msg.value);
    _postOrder(_buy, _closingTime, _numberOfTokens, _priceWei, _token, msg.sender);
  }

  function _deleteOrder(bytes32 _hash) internal {
    delete orders[_hash];
    emit OrderDeleted(_hash);
  }

  function _postOrder(bool _buy, uint256 _closingTime, uint256 _numberOfTokens, uint256 _priceWei, IERC20 _token, address _from) internal {
    bytes32 hash = keccak256(abi.encodePacked(_buy, _closingTime, _numberOfTokens, _priceWei, _token, _from));
    orders[hash] = Order(_buy, _closingTime, _numberOfTokens, _priceWei, _token, _from);
    require(isValid(hash));

    emit OrderPosted(hash, _buy, _closingTime, _numberOfTokens, _priceWei, _token, _from);
  }
}
