# stx-contracts
The [smart contracts](https://en.wikipedia.org/wiki/Smart_contract) powering STX

Built on the Ethereum blockchain, Solidity, and OpenZeppelin:
- using common contract security patterns (See [Onward with Ethereum Smart Contract Security](https://medium.com/bitcorps-blog/onward-with-ethereum-smart-contract-security-97a827e47702#.y3kvdetbz))
- in the [Solidity language](https://solidity.readthedocs.io/en/develop/).

> NOTE: New to smart contract development? Check OpenZeppelin's [introductory guide](https://medium.com/zeppelin-blog/the-hitchhikers-guide-to-smart-contracts-in-ethereum-848f08001f05#.cox40d2ut).

## Getting Started

STX integrates with [Truffle](https://github.com/ConsenSys/truffle).

## Installing stx-contracts

After installing Truffle, to install the blockimmo platform smart contracts, run the following in your Solidity project root directory:

```sh
cd stx-contracts
npm i
sh scripts/test.sh
```

### Smart contracts

| Contract                                                         | Version                                                                                                                               | Description                                                                                                               |
| --------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| [`Exchange`](/contracts/Exchange.sol)                                      | v0.0.1                                                 | A minimal, simple smart contract to place bids and asks (orders).                                                                 |

## Tests
Unit test are critical to the STX platform. They help ensure code quality and mitigate against security vulnerabilities. The directory structure within the `/tests` directory corresponds to the `/contracts` directory. blockimmo uses Mocha’s JavaScript testing framework and Chai’s assertion library. To learn more about how to tests are structured, please reference [OpenZeppelin’s Testing Guide](https://github.com/OpenZeppelin/openzeppelin-solidity#tests).

## Testing notes
Some of our contracts rely on the addresses of the deployed `LandRegistryProxy`, and `WhitelistProxy` smart contracts. These addresses differ between testing, Ropsten, and Main net. Comments in our smart contracts indicate which addresses must be used for each kind of deployment.

## Security
STX is currently in beta and not production ready yet.

We follow the core development principles and strategies that OpenZeppelin is based on including: security in depth, simple and modular code, clarity-driven naming conventions, comprehensive unit testing, pre-and-post-condition sanity checks, code consistency, and regular audits.

If you find a security issue, please make an issue at our [HackerOne page](https://hackerone.com/blockimmo).

## License
Copyright (c) 2018 STX AG license@stx.swiss
