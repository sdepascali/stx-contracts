const { balanceDifference } = require('openzeppelin-solidity/test/helpers/balanceDifference');
const shouldFail = require('openzeppelin-solidity/test/helpers/shouldFail');

const LandRegistry = artifacts.require('LandRegistry');
const LandRegistryProxy = artifacts.require('LandRegistryProxy');
const TokenizedProperty = artifacts.require('TokenizedProperty');
const Whitelist = artifacts.require('Whitelist');
const WhitelistProxy = artifacts.require('WhitelistProxy');

const Exchange = artifacts.require('Exchange');

require('chai')
  .should();

contract('Exchange', function ([from, other, unauthorized]) {
  const defaultClosingTime = parseInt(Date.now() / 1000, 10) + 3600;

  before(async function () {
    this.landRegistryProxy = await LandRegistryProxy.new({ from });
    this.landRegistryProxy.address.should.be.equal('0x0f5ea0a652e851678ebf77b69484bfcd31f9459b');

    this.whitelistProxy = await WhitelistProxy.new({ from });
    this.whitelistProxy.address.should.be.equal('0xec8be1a5630364292e56d01129e8ee8a9578d7d8');

    this.landRegistry = await LandRegistry.new({ from });
    await this.landRegistryProxy.set(this.landRegistry.address);
    (await this.landRegistryProxy.landRegistry.call()).should.be.equal(this.landRegistry.address);

    this.whitelist = await Whitelist.new({ from });
    await this.whitelistProxy.set(this.whitelist.address);
    (await this.whitelistProxy.whitelist.call()).should.be.equal(this.whitelist.address);

    await this.whitelist.grantPermissionBatch([from, other], 'authorized');

    this.exchange = await Exchange.new();

    this.tokenizedProperty = await TokenizedProperty.new('CH12345678910', 'CH-ZG1234', { from });
    await this.landRegistry.tokenizeProperty('CH12345678910', this.tokenizedProperty.address, { from });
  });

  context('token is not whitelisted', function () {
    it('', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tokenizedProperty.address, { from }));
    });
  });

  context('token is whitelisted', function () {
    before(async function () {
      await this.whitelist.grantPermission(this.tokenizedProperty.address, 'authorized');
    });

    it('invalid closing time', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, parseInt(Date.now() / 1000, 10), 1e18, 1e18, this.tokenizedProperty.address, { from }));
    });

    it('invalid closing time', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, parseInt(Date.now() / 1000, 10) + 3600 * 24 * 8, 1e18, 1e18, this.tokenizedProperty.address, { from }));
    });

    it('invalid number of tokens', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, defaultClosingTime, 0, 1e18, this.tokenizedProperty.address, { from }));
    });

    it('invalid price wei', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, defaultClosingTime, 1e18, 0, this.tokenizedProperty.address, { from }));
    });

    it('seller does not hold enough tokens to fill this order', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, defaultClosingTime, 1e18, 1.1e24, this.tokenizedProperty.address, { from }));
    });

    it('seller has not approved enough tokens to fill this order', async function () {
      await shouldFail.reverting(this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tokenizedProperty.address, { from }));
    });

    it('invalid price wei and value', async function () {
      await shouldFail.reverting(this.exchange.postOrder(true, defaultClosingTime, 1e18, 1e18, this.tokenizedProperty.address, { from: other, value: 1.1e18 }));
    });
  });

  context('allowance is approved', function () {
    before(async function () {
      await this.tokenizedProperty.approve(this.exchange.address, 1e18, { from });
    });

    it('posts an order', async function () {
      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tokenizedProperty.address, { from });

      logs[0].event.should.be.equal('OrderPosted');
      (+logs[0].args.closingTime).should.be.equal(defaultClosingTime);
      (+logs[0].args.numberOfTokens).should.be.equal(1e18);
      (+logs[0].args.priceWei).should.be.equal(1e18);
      logs[0].args.token.should.be.equal(this.tokenizedProperty.address);
      logs[0].args.from.should.be.equal(from);

      const { order } = logs[0].args;

      await this.exchange.deleteOrder(order, { from });
    });

    it('deletes an order', async function () {
      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tokenizedProperty.address, { from });
      const { order } = logs[0].args;

      const r = await this.exchange.deleteOrder(order, { from });
      r.logs[0].event.should.be.equal('OrderDeleted');
      r.logs[0].args.order.should.be.equal(order);
    });

    it('another account can\'t delete a valid order', async function () {
      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tokenizedProperty.address, { from });
      const { order } = logs[0].args;

      await shouldFail.reverting(this.exchange.deleteOrder(order, { from: other }));
      await this.exchange.deleteOrder(order, { from });
    });

    it('duplicate orders', async function () {
      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tokenizedProperty.address, { from });
      logs[0].event.should.be.equal('OrderPosted');
      const { order } = logs[0].args;

      const r = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tokenizedProperty.address, { from });
      const _order = r.logs[0].args.order;

      order.should.be.equal(_order);
    });

    it('multiple orders', async function () {
      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e17, 1e18, this.tokenizedProperty.address, { from });
      logs[0].event.should.be.equal('OrderPosted');
      const { order } = logs[0].args;

      const r = await this.exchange.postOrder(false, defaultClosingTime, 9e17, 1e18, this.tokenizedProperty.address, { from });
      const _order = r.logs[0].args.order;

      order.should.not.equal(_order);
    });
  });

  context('trading', function () {
    beforeEach(async function () {
      this.tP = await TokenizedProperty.new('CH12345678911', 'CH-ZG1235', { from });
      await this.landRegistry.tokenizeProperty('CH12345678911', this.tP.address, { from });
      await this.whitelist.grantPermission(this.tP.address, 'authorized');
    });

    afterEach(async function () {
      await this.landRegistry.untokenizeProperty('CH12345678911', { from });
    });

    it('empty', async function () {
      await this.tP.approve(this.exchange.address, 1e18, { from });

      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tP.address, { from, gasPrice: 0 });

      const { order } = logs[0].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      (+(await balanceDifference(other, () =>
        this.exchange.fillOrders([], 0, { from: other, gasPrice: 0, value: 1e18 }))
      )).should.be.equal(0);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(0);
    });

    it('fills an order', async function () {
      await this.tP.approve(this.exchange.address, 1e18, { from });

      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tP.address, { from, gasPrice: 0 });

      const { order } = logs[0].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      (+(await balanceDifference(from, () =>
        this.exchange.fillOrders([order], 0, { from: other, value: 1e18 }))
      )).should.be.equal(1e18);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24 - 1e18);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(1e18);
    });

    it('partially fills an order', async function () {
      await this.tP.approve(this.exchange.address, 1e18, { from });

      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tP.address, { from, gasPrice: 0 });

      const { order } = logs[0].args;

      (+(await balanceDifference(from, () =>
        this.exchange.fillOrders([order], 0, { from: other, value: 1e18 / 2 }))
      )).should.be.equal(1e18 / 2);

      (await this.exchange.isValid(order)).should.be.equal(false);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24 - 1e18 / 2);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(1e18 / 2);
    });

    it('order filled by multiple parties', async function () {
      await this.tP.approve(this.exchange.address, 1e18, { from });

      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tP.address, { from, gasPrice: 0 });
      const { order } = logs[0].args;

      const r = await this.exchange.fillOrders([order], 0, { from: other, value: 1e18 / 2 });
      r.logs[0].event.should.be.equal('OrderFilled');
      r.logs[1].event.should.be.equal('OrderDeleted');
      r.logs[2].event.should.be.equal('OrderPosted');

      const _order = r.logs[2].args.order;
      (await this.exchange.isValid(_order)).should.be.equal(true);

      (+(await balanceDifference(from, () =>
        this.exchange.fillOrders([_order], 0, { from: other, value: 1e18 }))
      )).should.be.equal(1e18 / 2);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24 - 1e18);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(1e18);
    });

    it('buyer is refunded change', async function () {
      await this.tP.approve(this.exchange.address, 1e18, { from });

      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18 / 2, this.tP.address, { from, gasPrice: 0 });
      const { order } = logs[0].args;

      (+(await balanceDifference(other, () =>
        this.exchange.fillOrders([order], 0, { from: other, value: 1e18, gasPrice: 0 }))
      )).should.be.equal(-1e18 / 2);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24 - 1e18);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(1e18);
    });

    it('fills multiple orders', async function () {
      await this.tP.approve(this.exchange.address, 1e18, { from });

      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e17, 1e17, this.tP.address, { from, gasPrice: 0 });
      const { order } = logs[0].args;

      const r = await this.exchange.postOrder(false, defaultClosingTime, 9e17, 9e17, this.tP.address, { from, gasPrice: 0 });
      const _order = r.logs[0].args.order;

      (+(await balanceDifference(from, () =>
        this.exchange.fillOrders([order, _order], 0, { from: other, value: 1e18 }))
      )).should.be.equal(1e18);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24 - 1e18);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(1e18);
    });

    it('non whitelisted address can\'t fill an order', async function () {
      await this.tP.approve(this.exchange.address, 1e18, { from });
      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tP.address, { from, gasPrice: 0 });
      const { order } = logs[0].args;
      await shouldFail.reverting(this.exchange.fillOrders([order], 0, { from: unauthorized, value: 1e18 }));
    });

    it('fills multiple orders', async function () {
      await this.tP.transfer(other, 1e18, { from });

      await this.tP.approve(this.exchange.address, 1e18, { from });
      await this.tP.approve(this.exchange.address, 1e18, { from: other });

      const { logs } = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 1e18, this.tP.address, { from, gasPrice: 0 });
      const { order } = logs[0].args;

      const r = await this.exchange.postOrder(false, defaultClosingTime, 1e18, 2e18, this.tP.address, { from: other, gasPrice: 0 });
      const _order = r.logs[0].args.order;

      await this.whitelist.grantPermission(unauthorized, 'authorized');

      (+(await balanceDifference(from, () =>
        this.exchange.fillOrders([order, _order], 0, { from: unauthorized, value: 3e18 }))
      )).should.be.equal(1e18);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24 - 2e18);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(0);
      (+(await this.tP.balanceOf.call(unauthorized))).should.be.equal(2e18);
    });
  });

  context('buy orders', function () {
    beforeEach(async function () {
      this.tP = await TokenizedProperty.new('CH12345678911', 'CH-ZG1235', { from });
      await this.landRegistry.tokenizeProperty('CH12345678911', this.tP.address, { from });
      await this.whitelist.grantPermission(this.tP.address, 'authorized');
    });

    afterEach(async function () {
      await this.landRegistry.untokenizeProperty('CH12345678911', { from });
    });

    it('empty', async function () {
      const { logs } = await this.exchange.postOrder(true, defaultClosingTime, 1e18, 1e18, this.tP.address, { from: other, gasPrice: 0, value: 1e18 });

      const { order } = logs[0].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      await this.tP.approve(this.exchange.address, 1e18, { from });

      (+(await balanceDifference(other, () =>
        this.exchange.fillOrders([], 1e18, { from, gasPrice: 0 }))
      )).should.be.equal(0);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(0);
    });

    it('fills an order', async function () {
      const { logs } = await this.exchange.postOrder(true, defaultClosingTime, 1e18, 1e18, this.tP.address, { from: other, gasPrice: 0, value: 1e18 });

      const { order } = logs[0].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      await this.tP.approve(this.exchange.address, 1e18, { from });

      (+(await balanceDifference(from, () =>
        this.exchange.fillOrders([order], 1e18, { from, gasPrice: 0 }))
      )).should.be.equal(1e18);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24 - 1e18);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(1e18);
    });

    it('partially fills an order', async function () {
      const { logs } = await this.exchange.postOrder(true, defaultClosingTime, 1e18, 1e18, this.tP.address, { from: other, gasPrice: 0, value: 1e18 });

      const { order } = logs[0].args;

      await this.tP.approve(this.exchange.address, 1e18, { from });

      (+(await balanceDifference(from, () =>
        this.exchange.fillOrders([order], 1e18 / 2, { from, gasPrice: 0 }))
      )).should.be.equal(1e18 / 2);

      (await this.exchange.isValid(order)).should.be.equal(false);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24 - 1e18 / 2);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(1e18 / 2);
    });

    it('order filled by multiple parties', async function () {
      const { logs } = await this.exchange.postOrder(true, defaultClosingTime, 1e18, 1e18, this.tP.address, { from: other, gasPrice: 0, value: 1e18 });
      const { order } = logs[0].args;

      await this.tP.approve(this.exchange.address, 1e18, { from, gasPrice: 0 });

      const r = await this.exchange.fillOrders([order], 1e18 / 2, { from });
      r.logs[0].event.should.be.equal('OrderFilled');
      r.logs[1].event.should.be.equal('OrderDeleted');
      r.logs[2].event.should.be.equal('OrderPosted');

      const _order = r.logs[2].args.order;
      (await this.exchange.isValid(_order)).should.be.equal(true);

      (+(await balanceDifference(from, () =>
        this.exchange.fillOrders([_order], 1e18, { from, gasPrice: 0 }))
      )).should.be.equal(1e18 / 2);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24 - 1e18);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(1e18);
    });

    it('fills multiple orders', async function () {
      const { logs } = await this.exchange.postOrder(true, defaultClosingTime, 1e18, 1e18, this.tP.address, { from: other, gasPrice: 0, value: 1e18 });
      const { order } = logs[0].args;

      const r = await this.exchange.postOrder(true, defaultClosingTime, 1e18, 2e18, this.tP.address, { from: unauthorized, gasPrice: 0, value: 2e18 });
      const _order = r.logs[0].args.order;

      await this.whitelist.grantPermission(unauthorized, 'authorized');

      await this.tP.approve(this.exchange.address, 2e18, { from });

      (+(await balanceDifference(from, () =>
        this.exchange.fillOrders([order, _order], 2e18, { from, gasPrice: 0 }))
      )).should.be.equal(3e18);

      (+(await this.tP.balanceOf.call(from))).should.be.equal(1e24 - 2e18);
      (+(await this.tP.balanceOf.call(other))).should.be.equal(1e18);
      (+(await this.tP.balanceOf.call(unauthorized))).should.be.equal(1e18);
    });

    it('deletes an order', async function () {
      const { logs } = await this.exchange.postOrder(true, defaultClosingTime, 1e18, 1e18, this.tP.address, { from: other, gasPrice: 0, value: 1e18 });

      const { order } = logs[0].args;
      (await this.exchange.isValid(order)).should.be.equal(true);

      (+(await balanceDifference(other, () =>
        this.exchange.deleteOrder(order, { from: other, gasPrice: 0 }))
      )).should.be.equal(1e18);

      (+(await this.tP.balanceOf.call(other))).should.be.equal(0);
    });
  });
});
